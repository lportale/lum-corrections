import os
import envyaml
from analysis_tools.utils import import_root

from PhysicsTools.NanoAODTools.postprocessing.modules.common.puWeightProducer import (
    puWeight_2016, puWeight_2017, puWeight_2018, puWeight_UL2016, puWeight_UL2017, puWeight_UL2018
)

ROOT = import_root()

puWeightFiles = envyaml.EnvYAML('%s/src/Corrections/LUM/python/puWeightFiles.yaml'%os.environ['CMSSW_BASE'])

def puWeight(**kwargs):
    isMC = kwargs.pop("isMC")
    year = int(kwargs.pop("year"))
    isUL = kwargs.pop("isUL")
    ul = "" if not isUL else "UL"

    if not isMC:
        return lambda: DummyModule(**kwargs)
    else:
        if year == 2016:
            return eval("puWeight_%s2016" % ul)
        elif year == 2017:
            return eval("puWeight_%s2017" % ul)
        elif year == 2018:
            return eval("puWeight_%s2018" % ul)


class puWeightRDFProducer():
    def __init__(self, myfile, targetfile, myhist="pileup", targethist="pileup", name="puWeight",
            norm=True, verbose=False, nvtx_var="Pileup_nTrueInt", doSysVar=True, automatic=False, *args, **kwargs):
        self.name = name
        self.nvtx_var = nvtx_var
        self.doSysVar = doSysVar

        if "/libBaseModules.so" not in ROOT.gSystem.GetLibraries():
            ROOT.gSystem.Load("libBaseModules.so")
        base = "{}/src/Corrections/LUM".format(os.getenv("CMSSW_BASE"))

        if not os.getenv("_PUWEIGHT"):
            os.environ["_PUWEIGHT"] = "puweight"
            ROOT.gROOT.ProcessLine(".L {}/interface/puWeightinterface.h".format(base))

            ROOT.gInterpreter.Declare(
                'auto worker = puWeightinterface("%s", "%s", "%s", "%s", %s, %s, %s);' % (
                    myfile, targetfile, myhist, targethist, ("true" if norm else "false"),
                    ("true" if verbose else "false"), ("true" if automatic else "false"))
            )

            if self.doSysVar:
                ROOT.gInterpreter.Declare(
                    'auto worker_plus = puWeightinterface("%s", "%s", "%s", "%s", %s, %s, %s);' % (
                    myfile, targetfile, myhist, targethist + "_plus", ("true" if norm else "false"),
                    ("true" if verbose else "false"), ("true" if automatic else "false"))
                )
                ROOT.gInterpreter.Declare(
                    'auto worker_minus = puWeightinterface("%s", "%s", "%s", "%s", %s, %s, %s);' % (
                    myfile, targetfile, myhist, targethist + "_minus", ("true" if norm else "false"),
                    ("true" if verbose else "false"), ("true" if automatic else "false"))
                )

    def run(self, df):
        df = df.Define(self.name, "worker.get_weight(%s)" % self.nvtx_var)
        var_to_return = [self.name]
        
        if self.doSysVar:
            df = df.Define(self.name + "Up", "worker_plus.get_weight(%s)" % self.nvtx_var)
            df = df.Define(self.name + "Down", "worker_minus.get_weight(%s)" % self.nvtx_var)
            var_to_return += [self.name + "Up", self.name + "Down"]

        return df, var_to_return


class puWeightDummyRDFProducer():
    def run(self, df):
        return df, []


def puWeightRDF(**kwargs):
    isMC = kwargs.pop("isMC")
    year = int(kwargs.pop("year"))
    auto = kwargs.pop("auto")
    if auto: file = kwargs.pop("file")

    prefix = ""
    try:
        isUL = kwargs.pop("isUL")
        prefix = "L" if not isUL else "UL"
    except KeyError:
        pass
    try:
        prefix = kwargs.pop("runPeriod")
    except KeyError:
        pass

    if not isMC:
        return lambda: puWeightDummyRDFProducer()
    else:
        if auto:
            return lambda: puWeightRDFProducer(file, puWeightFiles["puFile_data"][prefix+str(year)], "pu_mc", "pileup", verbose=False, doSysVar=False, automatic=True)
        else:
            return lambda: puWeightRDFProducer(puWeightFiles["puFile_mc"][prefix+str(year)], puWeightFiles["puFile_data"][prefix+str(year)], "pu_mc", "pileup", verbose=False, doSysVar=False)
