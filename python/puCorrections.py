import os
import envyaml

from analysis_tools.utils import import_root
ROOT = import_root()

import correctionlib
correctionlib.register_pyroot_binding()

corrCfg = envyaml.EnvYAML('%s/src/Corrections/LUM/python/puCorrections.yaml' %
                                    os.environ['CMSSW_BASE'])

class puWeightRDFProducer():
    def __init__(self, *args, **kwargs):
        self.isMC = kwargs.pop("isMC")
        self.year = kwargs.pop("year") ; year = str(self.year)

        prefix = "" ; isUL = False
        try:
            isUL = kwargs.pop("isUL")
            prefix += "" if not isUL else "UL"
        except KeyError:
            pass
        try:
            prefix += kwargs.pop("runPeriod")
        except KeyError:
            pass

        self.corrKey = prefix+year

        if self.year < 2022 and not isUL:
            raise ValueError("Only implemented for Run 3 and UL Run 3 datasets.")

        if not os.getenv("_corr"):
            os.environ["_corr"] = "_corr"
            if "/libBaseModules.so" not in ROOT.gSystem.GetLibraries():
                ROOT.gInterpreter.Load("libBaseModules.so")
            ROOT.gInterpreter.Declare(os.path.expandvars(
                '#include "$CMSSW_BASE/src/Base/Modules/interface/correctionWrapper.h"'))

        if not os.getenv(f"_pu_{self.corrKey}"):
            os.environ[f"_pu_{self.corrKey}"] = "_pu"
            ROOT.gInterpreter.ProcessLine(
                'auto pu_corr_%s = MyCorrections("%s", "%s");' %
                    (self.corrKey, corrCfg[self.corrKey]["fileName"],
                        corrCfg[self.corrKey]["corrName"]))

        ROOT.gInterpreter.Declare("""
            Float_t get_pu_weight_%s(Float_t NumTrueInteractions, std::string type) {
                return pu_corr_%s.eval({NumTrueInteractions, type});
            }
        """ % (self.corrKey, self.corrKey))

    def run(self, df):
        if not self.isMC:
            return df, []

        branches = []
        for syst_name, syst in [("", "nominal"), ("_up", "up"), ("_down", "down")]:
            df = df.Define("puWeight%s" % syst_name,
                           'get_pu_weight_%s(Pileup_nTrueInt, "%s")' % (self.corrKey, syst))

            branches.append("puWeight%s" % syst_name)

        return df, branches

def puWeightRDF(**kwargs):
    """
    Module to apply veto maps that veto out events with important jets in the "hot" or
    "cold" zones. These maps should be applied similarly both on Data and MC, to keep the
    phase-spaces equal.

    YAML sintaxis:

    .. code-block:: yaml

        codename:
            name: jecProviderRDF
            path: Base.Modules.jec
            parameters:
                isMC: self.dataset.process.isMC
                year: self.config.year
                runPeriod: self.config.runPeriod
    """

    return lambda: puWeightRDFProducer(**kwargs)
