import os
import ROOT
ROOT.gROOT.SetBatch(True)

from Corrections.LUM.puCorrections import puWeightRDF
from Corrections.LUM.puWeightCor import puWeightRDF as puWeightRDF_old
from Corrections.LUM.puWeight import puWeightRDF as puWeightRDF_nocorlib
test_folder = "$CMSSW_BASE/src/Base/Modules/data/"

def pu_test_2016_preVFP(df, print_test=True):
    pu_2016_preVFP = puWeightRDF(
        isMC=True,
        year=2016,
        isUL=True,
        runPeriod="preVFP"
    )()
    df, _ = pu_2016_preVFP.run(df)
    if print_test:
        h = df.Histo1D("puWeight")
        print("PU 2016_preVFP puWeight Integral: %s, Mean: %s, Std: %s" % (
            h.Integral(), h.GetMean(), h.GetStdDev()))
    return df

def pu_test_2016_postVFP(df, print_test=True):
    pu_2016_postVFP = puWeightRDF(
        isMC=True,
        year=2016,
        isUL=True,
        runPeriod="postVFP"
    )()
    df, _ = pu_2016_postVFP.run(df)
    if print_test:
        h = df.Histo1D("puWeight")
        print("PU 2016_postVFP puWeight Integral: %s, Mean: %s, Std: %s" % (
            h.Integral(), h.GetMean(), h.GetStdDev()))
    return df

def pu_test_2017(df, print_test=True):
    pu_2017 = puWeightRDF(
        isMC=True,
        year=2017,
        isUL=True,
    )()
    df, _ = pu_2017.run(df)
    if print_test:
        h = df.Histo1D("puWeight")
        print("PU 2017 puWeight Integral: %s, Mean: %s, Std: %s" % (
            h.Integral(), h.GetMean(), h.GetStdDev()))
    return df

def pu_test_2018(df, print_test=True):
    pu_2018 = puWeightRDF(
        isMC=True,
        year=2018,
        isUL=True,
    )()
    df, _ = pu_2018.run(df)
    if print_test:
        h = df.Histo1D("puWeight")
        print("PU 2018 puWeight Integral: %s, Mean: %s, Std: %s" % (
            h.Integral(), h.GetMean(), h.GetStdDev()))
    return df

def pu_test_2018_old(df, print_test=True):
    pu_2018 = puWeightRDF_old(
        isMC=True,
        year=2018,
        isUL=True,
    )()
    df, _ = pu_2018.run(df)
    if print_test:
        h = df.Histo1D("puWeight2")
        print("PU 2018 puWeight (old version) Integral: %s, Mean: %s, Std: %s" % (
            h.Integral(), h.GetMean(), h.GetStdDev()))
    return df

def pu_test_2018_nocorlib(df, print_test=True):
    pu_2018 = puWeightRDF_nocorlib(
        isMC=True,
        year=2018,
        isUL=True,
        auto=False,
    )()
    df, _ = pu_2018.run(df)
    if print_test:
        h = df.Histo1D("puWeight")
        print("PU 2018 puWeight (no correctionlib) Integral: %s, Mean: %s, Std: %s" % (
            h.Integral(), h.GetMean(), h.GetStdDev()))
    return df

def pu_test_2022(df, print_test=True):
    pu_2022 = puWeightRDF(
        isMC=True,
        year=2022,
        runPeriod="preEE"
    )()
    df, _ = pu_2022.run(df)
    if print_test:
        h = df.Histo1D("puWeight")
        print("PU 2022_preEE puWeight Integral: %s, Mean: %s, Std: %s" % (
            h.Integral(), h.GetMean(), h.GetStdDev()))
    return df

def pu_test_2022_postEE(df, print_test=True):
    pu_2022_postEE = puWeightRDF(
        isMC=True,
        year=2022,
        runPeriod="postEE"
    )()
    df, _ = pu_2022_postEE.run(df)
    if print_test:
        h = df.Histo1D("puWeight")
        print("PU 2022_postEE puWeight Integral: %s, Mean: %s, Std: %s" % (
            h.Integral(), h.GetMean(), h.GetStdDev()))
    return df

def pu_test_2023(df, print_test=True):
    pu_2023 = puWeightRDF(
        isMC=True,
        year=2023,
        runPeriod="preBPix"
    )()
    df, _ = pu_2023.run(df)
    if print_test:
        h = df.Histo1D("puWeight")
        print("PU 2023_preBPix puWeight Integral: %s, Mean: %s, Std: %s" % (
            h.Integral(), h.GetMean(), h.GetStdDev()))
    return df

def pu_test_2023_postBPix(df, print_test=True):
    pu_2023_postBPix = puWeightRDF(
        isMC=True,
        year=2023,
        runPeriod="postBPix"
    )()
    df, _ = pu_2023_postBPix.run(df)
    if print_test:
        h = df.Histo1D("puWeight")
        print("PU 2023_postBPix puWeight Integral: %s, Mean: %s, Std: %s" % (
            h.Integral(), h.GetMean(), h.GetStdDev()))
    return df

if __name__ == "__main__":
    df_2016_preVFP = ROOT.RDataFrame("Events", os.path.expandvars(os.path.join(test_folder, "testfile_mc2016.root")))
    df_2016_preVFP = pu_test_2016_preVFP(df_2016_preVFP)

    df_2016_postVFP = ROOT.RDataFrame("Events", os.path.expandvars(os.path.join(test_folder, "testfile_mc2016_apv.root")))
    df_2016_postVFP = pu_test_2016_postVFP(df_2016_postVFP)

    df_2017 = ROOT.RDataFrame("Events", os.path.expandvars(os.path.join(test_folder, "testfile_mc2017.root")))
    df_2017 = pu_test_2017(df_2017)

    df_2018 = ROOT.RDataFrame("Events", os.path.expandvars(os.path.join(test_folder, "testfile_mc2018.root")))
    df_2018 = pu_test_2018(df_2018)
    df_2018 = pu_test_2018_old(df_2018)
    # need to recreate the dataframe to avoid variable redefinition
    df_2018 = ROOT.RDataFrame("Events", os.path.expandvars(os.path.join(test_folder, "testfile_mc2018.root")))
    df_2018 = pu_test_2018_nocorlib(df_2018)
    
    df_2022 = ROOT.RDataFrame("Events", os.path.expandvars(os.path.join(test_folder, "testfile_mc2022.root")))
    df_2022 = pu_test_2022(df_2022)

    df_2022_postEE = ROOT.RDataFrame("Events", os.path.expandvars(os.path.join(test_folder, "testfile_mc2022_postee.root")))
    df_2022_postEE = pu_test_2022_postEE(df_2022_postEE)

    df_2023 = ROOT.RDataFrame("Events", os.path.expandvars(os.path.join(test_folder, "testfile_mc2023.root")))
    df_2023 = pu_test_2023(df_2023)

    df_2023_postBPix = ROOT.RDataFrame("Events", os.path.expandvars(os.path.join(test_folder, "testfile_mc2023_postbpix.root")))
    df_2023_postBPix = pu_test_2023_postBPix(df_2023_postBPix)
